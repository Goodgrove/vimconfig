# Personal Vim Configuration files and plugins

.vimrc just the way I like it.

# Neovim

Copy `init.vim` to `~/.config/nvim/init.vim`

Install Plug:
    curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    
